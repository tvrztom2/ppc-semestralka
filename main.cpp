#include <iostream>
#include <vector>

void vykreslitPole(const std::vector<std::vector<char>>& pole) {
    // Funkce pro vykreslení hracího pole do konzole
    int radky = pole.size();
    int sloupce = pole[0].size();

    // Vykreslení horního okraje hracího pole
    std::cout << "+";
    for (int j = 0; j < sloupce + 1; ++j) {
        std::cout << "---+";
    }
    std::cout << std::endl;

    // Vykreslení čísel sloupců nad hracím polem
    std::cout << "|   |";
    for (int j = 0; j < sloupce; ++j) {
        std::cout << " " << j + 1 << " |";
    }
    std::cout << std::endl;

    // Vykreslení oddělovací linky mezi čísly sloupců a hracím polem
    std::cout << "+";
    for (int j = 0; j < sloupce + 1; ++j) {
        std::cout << "---+";
    }
    std::cout << std::endl;

    // Vykreslení hracího pole s obsahem
    char radekZnak = 'A';
    for (int i = 0; i < radky; ++i) {
        std::cout << "| " << radekZnak << " |";
        for (int j = 0; j < sloupce; ++j) {
            std::cout << " " << pole[i][j] << " |";
        }
        std::cout << std::endl;

        // Vykreslení oddělovací linky mezi řádky hracího pole
        std::cout << "+";
        for (int j = 0; j < sloupce + 1; ++j) {
            std::cout << "---+";
        }
        std::cout << std::endl;
        ++radekZnak;
    }
}

bool kontrolaVyhry(const std::vector<std::vector<char>>& pole, char hrac) {
    // Funkce pro kontrolu, zda hráč vyhrál
    int radky = pole.size();
    int sloupce = pole[0].size();

    // Kontrola diagonál
    for (int i = 0; i <= radky - 3; ++i) {
        for (int j = 0; j <= sloupce - 3; ++j) {
            if (pole[i][j] == hrac && pole[i + 1][j + 1] == hrac && pole[i + 2][j + 2] == hrac) {
                return true; // Výhra diagonálně zleva nahoru doprava
            }
            if (pole[i][j + 2] == hrac && pole[i + 1][j + 1] == hrac && pole[i + 2][j] == hrac) {
                return true; // Výhra diagonálně zprava nahoru doleva
            }
        }
    }

    // Kontrola řádků
    for (int i = 0; i < radky; ++i) {
        for (int j = 0; j <= sloupce - 3; ++j) {
            if (pole[i][j] == hrac && pole[i][j + 1] == hrac && pole[i][j + 2] == hrac) {
                return true; // Výhra v řádku
            }
        }
    }

    // Kontrola sloupců
    for (int i = 0; i <= radky - 3; ++i) {
        for (int j = 0; j < sloupce; ++j) {
            if (pole[i][j] == hrac && pole[i + 1][j] == hrac && pole[i + 2][j] == hrac) {
                return true; // Výhra ve sloupci
            }
        }
    }

    return false;
}

int main() {
    int radky, sloupce;

    // Načtení počtu řádků od uživatele
    do {
        std::cout << "Zadejte pocet radku (MAX 9): ";
        std::cin >> radky;
    } while (radky > 9);

    // Načtení počtu sloupců od uživatele
    do {
        std::cout << "Zadejte pocet sloupcu (MAX 9): ";
        std::cin >> sloupce;
    } while (sloupce > 9);

    // Inicializace hracího pole
    std::vector<std::vector<char>> pole(radky, std::vector<char>(sloupce, ' '));

    char aktualniHrac = 'X';

    while (true) {
        vykreslitPole(pole);

        char radek;
        int sloupec;

        // Načtení pozice od uživatele
        do {
            std::cout << "Hrac " << aktualniHrac << ", zadejte radek (A-X): ";
            std::cin >> radek;
            radek = std::toupper(radek);
        } while (radek < 'A' || radek > 'A' + radky - 1);

        do {
            std::cout << "Hrac " << aktualniHrac << ", zadejte sloupec (1-" << sloupce << "): ";
            std::cin >> sloupec;
        } while (sloupec < 1 || sloupec > sloupce);

        // Převod zadané pozice na indexy hracího pole
        int radekIndex = radek - 'A';

        // Kontrola platnosti zadané pozice
        if (radekIndex < 0 || radekIndex >= radky || sloupec < 1 || sloupec > sloupce) {
            std::cout << "Neplatna pozice. Zadejte znovu." << std::endl;
            continue;
        }

        // Kontrola obsazenosti zadané pozice
        if (pole[radekIndex][sloupec - 1] != ' ') {
            std::cout << "Pozice je jiz obsazena. Zadejte znovu." << std::endl;
            continue;
        }

        // Umístění značky hráče na pozici
        pole[radekIndex][sloupec - 1] = aktualniHrac;

        // Kontrola výhry
        if (kontrolaVyhry(pole, aktualniHrac)) {
            vykreslitPole(pole);
            std::cout << "Hrac " << aktualniHrac << " vyhral!" << std::endl;
            break;
        }

        // Přepnutí hráče
        aktualniHrac = (aktualniHrac == 'X') ? 'O' : 'X';
    }

    return 0;
}
